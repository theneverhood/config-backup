# shellcheck shell=bash
LOGFILE=${CONFIG_SYNC_LOG_FILE:-"configs/sync.log"}
DEBUG=${DEBUG:-"false"}

# Logging in logfmt format. Can be used for pretty log parsing via e.g. https://github.com/humanlogio/humanlog
log_info() {
  echo "level=info time=\"$(date)\" msg=\"$1\"" | tee -a "$LOGFILE"
}

log_error(){
  echo "level=error time=\"$(date)\" msg=\"$1\"" | tee -a "$LOGFILE"
}

log_debug(){
  echo "level=debug time=\"$(date)\" msg=\"$1\"" | tee -a "$LOGFILE"
}

# Git utils

git_root(){
    git rev-parse --show-toplevel
}