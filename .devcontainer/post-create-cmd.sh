#!/bin/bash

# Installing tools that are not maintained as devcontainer features (watchexec and scalyr). 
# This script is called on devcontainer's `postCreateCommand` hook. 
# see: https://containers.dev/implementors/json_reference/#lifecycle-scripts

# apt repo for rust tools, in this case specifically for watchexec-cli
# curl -fsSL https://apt.cli.rs/pubkey.asc | sudo tee -a /usr/share/keyrings/rust-tools.asc
# curl -fsSL https://apt.cli.rs/rust-tools.list | sudo tee /etc/apt/sources.list.d/rust-tools.list
# sudo apt update && apt install watchexec-cli

wget -O /usr/local/bin/lefthook https://github.com/evilmartians/lefthook/releases/download/v1.6.1/lefthook_1.6.1_Linux_aarch64 && chmod +x /usr/local/bin/lefthook
wget -O /usr/local/bin/scalyr https://raw.githubusercontent.com/scalyr/scalyr-tool/master/scalyr && chmod +x /usr/local/bin/scalyr