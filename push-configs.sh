#!/bin/bash
set -eo pipefail

# GIT_ROOT_PATH=$(git rev-parse --show-toplevel) # TODO put to .envrc?
CONFIG_DIR=${CONFIG_DIR:-"configs"} # TODO We should create the folder at some init script (plus the .gitkeep), otherwise it shouldn't be configurable.

source "utils.sh"

# Upload a specific file to Dataset
upload_file() {
  config_file=$1
  
  # Check if the configuration file exists in the local git repo
  if ! rg -Fx "$config_file" "$CONFIG_DIR/config_paths"; then
    log_debug "File $config_file not found in local git repo."
    return
  fi

  # Upload the content of the config file to Dataset
  log_info "Uploading file $config_file to Dataset."
  # TODO there is --validate flag in scalyr put-file cmd. It would make sense to use it and revert the commit if the config is not valid. (though from my quick testing, it just freezed.)
  scalyr put-file "$CONFIG_DIR/$config_file" 2>&1 | while IFS= read -r line
  do
    if echo "$line" | rg -q "error"; then
      log_error "$line"
    else
      log_info "$line"
    fi
  done
}

# Function to check if a specific file exists in the list of files pulled from Dataset.
# This is ofc wrong, but with limited scope, i didn't really study the api and how it handles unknown configs at unknown paths. (though the `example` indicates that it doesn't care ¯\_(ツ)_/¯)
file_exists() {
  file=$1
  
  if ! rg -Fx "$file" "$CONFIG_DIR/config_paths"; then
    return 1 # File not found in Dataset
  fi
  
  return 0 # File found in Dataset
}

# Function to push changes to Dataset
push_changes_to_dataset() {
  # Get a list of all files changed in the last commit
  changed_files=$(git diff --name-only HEAD^ HEAD)

  for file in $changed_files; do
    # Check if the file exists in Dataset's config files
    if file_exists "$file"; then
      upload_file "$file"
    fi
  done
}

# Function to manually trigger commiting changes
manual_commit() {
  # Disable Lefthook so we don't trigger the hooks - we're handling the logic here.
  export LEFTHOOK=0

  # Stage all changes
  git add .

  # Check if there are any changes that are not in the repository
  if git diff --cached | read; then
    # Attempt to commit changes with merge. (At least some kind of conflict error propagation)
    git commit -m "Commit added via config-sync function."

    # Check if a conflict occurred during the commit
    if [ $? -ne 0 ]; then
      # Abort the commit
      git merge --abort
      log_error "A conflict occurred during the commit. Please update your local repository."
    else
      push_changes_to_dataset
    fi
  else
    log_error "No changes to commit."
  fi

  # Enable Lefthook
  unset LEFTHOOK
}

# Poor mans cli - check for arguments to allow for manual trigger of commit instead of the post-commit hook. Just in case.
while getopts ":m" opt; do
  case ${opt} in
    m)
      manual_commit
      ;;
    \?)
      echo "Invalid option: $OPTARG" 1>&2
      ;;
  esac
done

# If no arguments were provided, push changes to dataset
if [ $# -eq 0 ]; then
  push_changes_to_dataset
fi