#!/bin/bash

source .env
source "utils.sh"

CONFIG_DIR=${CONFIG_DIR:-"configs"} 
CONFIG_PATHS="${CONFIG_DIR}/config_paths" # File to store the list of config files from Dataset

# Dump all config files paths to file, so we can reference it later.
list_files() {
  scalyr list-files > "$CONFIG_PATHS"
}

# Sync a specific file from Dataset to local git repo
sync_file() {
  local config_file=$1
  local config_file_path="${CONFIG_DIR}/${config_file}"
  local updated_files=0

  # Check if the configuration file exists in Dataset using ripgrep (performance, respecting .gitignore )
  if ! rg -qFx "$config_file" "$CONFIG_PATHS"; then
    log_info "File $config_file not found in Dataset."
    return
  fi

  # Fetch the content of the config file from Dataset
  log_info "Fetching file $config_file"
  if ! scalyr_content=$(scalyr get-file "$config_file"); then
    log_info "Failed to fetch file $config_file from Dataset."
    return
  fi

  # If the local file does not exist or differs from the Dataset version, update it
  if [ ! -f "$config_file_path" ] || ! echo "$scalyr_content" | cmp -s "$config_file_path" -; then
    log_info "Local file $config_file is different or does not exist. Updating local copy."
    # Ensure the directory exists before writing the file
    mkdir -p "$(dirname "$config_file_path")"
    echo "$scalyr_content" > "$config_file_path"
    git add "$config_file_path"
    updated_files=$((updated_files + 1))
  else
    log_info "No changes in local file $config_file, skipping update."
  fi

  return $updated_files
}

main() {
  list_files

  local total_updated_files=0
  # Temporarily disable exit on error
  set +e
  # Read the config file paths (fetched via list-files previously), loops over and gets contents of the config files.
  while IFS= read -r config_file; do
    sync_file "$config_file"
    total_updated_files=$((total_updated_files + $?))
  done < "$CONFIG_PATHS"
  # Re-enable exit on error
  set -e

  if [ "$total_updated_files" -gt 0 ]; then
    if pushd "$CONFIG_DIR" > /dev/null; then
      git commit -m "Automated backup of remotely updated config files"
      popd > /dev/null || log_error "Failed to revert to the previous directory."
      log_info "Successfully synced and committed remote config"
    else
      log_error "Failed to change directory to ${CONFIG_DIR}."
    fi
  else
    log_debug "No changes detected, skipping commit."
  fi
}

# Poor mans polling. Interval can be set via env variable.
while true; do
  main
  sleep "${POLLING_INTERVAL:-60}"
done